package io.eurekademo.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Flux;

@RestController
@RequestMapping("ElasticService")
public class EsCotroller {
    
    @RequestMapping(value = "GetAllUser",method = RequestMethod.GET)
    public Flux<Object> GetAllUser(){
        WebClient webClient = WebClient.create("http://localhost:8080");

        return webClient
            .get()
            .uri("/Ideas-ElasticSearch-Serivce/User/GetAll")
            .retrieve()
            .bodyToFlux(Object.class);

    }

}
